Declaring a CMake extension
===========================

.. autosummary::

  cmake_pip.cmake_extension.ExtensionCMake
  cmake_pip.cmake_extension.build_cmake
  cmake_pip.cmake_extension.setup


Reference
---------

.. automodule:: cmake_pip.cmake_extension
   :members: