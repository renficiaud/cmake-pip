
from cmake_pip.cmake_extension import ExtensionCMake, setup, build_ext

# extension created by cmake
ext1 = ExtensionCMake('distutils_cmake_test1.cmake_test',
                      'cmake_project1/CMakeLists.txt',
                      cmake_locate_extensions=True)

setup(name='distutils-cmake-test1',
      version='0.1a',
      packages=['distutils_cmake_test1'],
      package_dir={'distutils_cmake_test1': 'src'},
      ext_modules=[ext1],
      author='Raffi Enficiaud',
      author_email='raffi.enficiaud@free.fr',
      url='https://bitbucket.org/renficiaud/distutils-cmake',
      description='distutils-cmake-test1',
      license='Unlicensed',
      #cmdclass=cmdclass,
      #** additional_kwargs
      )
