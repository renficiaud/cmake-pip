#ifndef PYTHON_HEADER_HPP__
#define PYTHON_HEADER_HPP__

/*!@file
 * This file hijacks the inclusion of the python libraries on Windows to
 * prevent the linking with the debug version of python.lib (that is named
 * python_d.lib and that is not provided by default).
 */

#if defined(_WIN32) && defined(_DEBUG)
  #define PYTHON_H_HIJACK_AUTO_LINK
  #undef _DEBUG
#endif

#include <Python.h>

#if defined(PYTHON_H_HIJACK_AUTO_LINK)
  #define _DEBUG
  #undef PYTHON_H_HIJACK_AUTO_LINK
#endif

#endif /* PYTHON_HEADER_HPP__ */
