#include "python_header.hpp"

PyObject * test(PyObject *self, PyObject *args)
{
  return Py_BuildValue("i", 42);
}

static PyMethodDef cmake_test_definitions[] = {
  {"test",  test, METH_VARARGS, "test function"},
  {NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC initcmake_test()
{
  (void)Py_InitModule("cmake_test", cmake_test_definitions);
}
