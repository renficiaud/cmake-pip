cmake-pip
=========

**CMake bindings to distutils/setuptools/pip made simple.**

This project makes easy the creation and distribution of python packages for
code that uses CMake.

Why using CMake
---------------
Although a lot of effort has been made on the distutils/setuptools side,
[CMake](http://www.cmake.org) is a wonderful opensource project providing
a huge amount of functionality for creating builds and packages in a cross
platform way. CMake is very well suited for

* projects with a lot of source files
* C/C++ projects, with or without python extensions
* binding to a lot of different technologies (libtiff to Matlab) from the same project
* handling complex configuration and settings
* and many other appealing features

Documentation
-------------
You may find the full documentation on [read the docs](http://distutils-cmake.readthedocs.io/en/latest/).

How to use
----------
The interface is made very simple:

1. use directives to declare the content of a python package, on the cmake side
1. use a specific extension on the ``setup.py`` side to declare a source tree
   build upon CMake

   
TODOs
-----

* source distributions
* GIT 